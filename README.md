# Bahikhata

<b>Shiny</b> Toys, <br>
<b>Comfortable</b> Stays,<br>
<b>Luxury</b> Rides, <br>
<b>Crazy</b> Parties,<br>
<b>Yummy</b> Food,<br>
<b>Sensuous</b> Music,<br>
<b>That Sexy</b> Dress,<br>
<b>Can't put it down</b> Game.<br>

There is always a Dream running the show.<br>

A Dream of making something great.<br>
A Dream of a better future.<br>

We stand to protect those Dreams.<br>

What’s your <b>DREAM</b> ?
