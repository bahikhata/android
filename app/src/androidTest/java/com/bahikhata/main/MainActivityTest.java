package com.bahikhata.main;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.bahikhata.ActivityRule;
import com.bahikhata.R;
import com.bahikhata.activity.MainActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;

/**
 * Created by abhijeet on 19/07/16.
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    MainActivity mainActivity;

    @Rule
    public final ActivityRule<MainActivity> rule = new ActivityRule<>(MainActivity.class);

    @Before
    public void init(){
        mainActivity = rule.get();
    }

    @Test
    public void testShouldLaunchTxActivityWhenAddTxButtonClicked(){

        onView(withId(R.id.myFAB)).perform(click());
        onView(withId(R.id.account)).check(matches(isDisplayed()));

    }

}
