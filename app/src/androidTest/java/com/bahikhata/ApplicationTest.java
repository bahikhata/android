package com.bahikhata;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.test.FlakyTest;
import android.test.suitebuilder.annotation.MediumTest;
import android.test.suitebuilder.annotation.SmallTest;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {

    public ApplicationTest() {
        super(Application.class);
    }


    @SmallTest
    public void validateSecondActivity(){

    }

    @FlakyTest
    @MediumTest
    public void validateSecondActivityAgain(){

    }

}