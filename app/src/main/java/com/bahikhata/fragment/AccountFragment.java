package com.bahikhata.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.bahikhata.MyApplication;
import com.bahikhata.R;
import com.bahikhata.activity.AccTxnsActivity;
import com.bahikhata.adapter.AccountAdapter;
import com.bahikhata.db.DbHelper;
import com.bahikhata.model.Account;

/**
 * Created by abhijeet on 05/01/16.
 */
public class AccountFragment extends Fragment {
    ListView listView;
    AccountAdapter accountAdapter;
    private static final String TAG = AccountFragment.class.getSimpleName();
    DbHelper dbHelper;

    AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Account account = (Account)parent.getAdapter().getItem(position);
            Intent intent = new Intent(MyApplication.mContext, AccTxnsActivity.class);
            intent.putExtra("accountId", account.getId());
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.fade_out);
        }
    };

    AdapterView.OnItemLongClickListener longClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            Account account = (Account)parent.getAdapter().getItem(position);

            showDialog(view,account.getId());
            return true;
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account,
                container, false);

        listView = view.findViewById(R.id.listview);
        dbHelper = new DbHelper();

        fillData();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        dbHelper.closeDb();

    }

    private void showDialog(View view, final int objectId) {
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_acc,null);
        alertDialog.setView(dialogView);

        dialogView.findViewById(R.id.rename).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(objectId > 0){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    View dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_newacc,null);
                    final EditText name = (EditText)dialogView.findViewById(R.id.accName);
                    name.setText(dbHelper.fetchAccountById(objectId).getName());
                    name.setSelection(name.getText().length());
                    builder.setView(dialogView)
                            .setTitle("Rename Account")
                            .setPositiveButton("CHANGE", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if(name.getText().toString().isEmpty()) {
                                        name.setError(getString(R.string.error_name));
                                    }
                                    else if(dbHelper.checkIfAccountExists(name.getText().toString())) {
                                        name.setError(getString(R.string.error_account_multiple));
                                    }
                                    else{
                                        dbHelper.updateAccount(objectId,null,name.getText().toString());
                                        fillData();
                                        dialog.dismiss();
                                    }
                                }
                            });

                    // Create the AlertDialog object and return it
                    Dialog d = builder.create();
                    d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                    d.show();
                }
                alertDialog.dismiss();
            }
        });
        dialogView.findViewById(R.id.hide).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbHelper.deleteAccount(objectId);
                fillData();
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    public void fillData(){
        accountAdapter = new AccountAdapter(MyApplication.mContext, dbHelper.fetchAccounts());
        listView.setAdapter(accountAdapter);
        listView.setOnItemClickListener(listener);
        listView.setOnItemLongClickListener(longClickListener);
    }
}
