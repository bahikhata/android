package com.bahikhata.fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.widget.PopupMenu;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.fragment.app.Fragment;

import com.bahikhata.MyApplication;
import com.bahikhata.R;
import com.bahikhata.activity.AccTxnsActivity;
import com.bahikhata.adapter.CashAdapter;
import com.bahikhata.db.DbHelper;
import com.bahikhata.listener.OnSwipeTouchListener;
import com.bahikhata.model.Transaction;
import com.bahikhata.util.CommonUtils;
import com.shamanland.fonticon.FontIconDrawable;
import org.joda.time.DateTime;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by abhijeet on 05/01/16.
 */
public class CashFragment extends Fragment {
    TextView dateSelect;
    ViewFlipper viewFlipper;
    ListView incomeList;
    ListView expenseList;
    LinearLayout dailyCash;
    FloatingActionButton myFAB;

    private static final String TAG = CashFragment.class.getSimpleName();
    static SimpleDateFormat formatter = CommonUtils.getDateFormat();
    static DateTime dt = new DateTime();
    View openBalance,closeBalance;
    DbHelper dbHelper;

    AdapterView.OnItemLongClickListener longClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            Transaction transaction = (Transaction) parent.getAdapter().getItem(position);

            if (!transaction.getCancelled() && transaction.getId() > 0) {
                showDialog(view,transaction);
            }

            return true;
        }
    };

    AdapterView.OnTouchListener touchListener =  new OnSwipeTouchListener(MyApplication.mContext) {
        @Override
        public void onSwipeRight() {
            super.onSwipeRight();
            dt = dt.minusDays(1);
            dateSelect.setText(formatter.format(dt.toDate()));

            fillData();

            viewFlipper.setInAnimation(MyApplication.mContext, R.anim.slide_in_left);
            viewFlipper.setOutAnimation(MyApplication.mContext, R.anim.fade_out_quick);
            viewFlipper.showNext();
        }

        @Override
        public void onSwipeLeft() {
            super.onSwipeLeft();

            dt = dt.plusDays(1);
            if (!dt.isAfterNow()) {
                dateSelect.setText(formatter.format(dt.toDate()));

                fillData();

                viewFlipper.setInAnimation(MyApplication.mContext, R.anim.slide_in_right);
                viewFlipper.setOutAnimation(MyApplication.mContext, R.anim.fade_out_quick);
                viewFlipper.showNext();
            } else {
                dt = dt.minusDays(1);
            }
        }
    };

    View.OnClickListener dateListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Calendar newCalendar = Calendar.getInstance();

            DatePickerDialog datePickerDialog = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    dt = new DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0, 0);
                    dateSelect.setText(formatter.format(dt.toDate()));

                    fillData();

                }
            }, newCalendar.get(Calendar.YEAR)
                    , newCalendar.get(Calendar.MONTH)
                    , newCalendar.get(Calendar.DAY_OF_MONTH));

            datePickerDialog.getDatePicker().setMaxDate(new DateTime().toDate().getTime());
            datePickerDialog.show();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cash,
                container, false);

        dateSelect = (TextView)view.findViewById(R.id.dateSelect);
        viewFlipper = (ViewFlipper)view.findViewById(R.id.viewFlipper);
        incomeList = (ListView)view.findViewById(R.id.income);
        expenseList = (ListView)view.findViewById(R.id.expense);
        dailyCash = (LinearLayout)view.findViewById(R.id.dailyCash);
        myFAB = (FloatingActionButton)view.findViewById(R.id.myFAB);

        dbHelper = new DbHelper();

        openBalance = getLayoutInflater(null).inflate(R.layout.txn, incomeList, false);
        closeBalance = getLayoutInflater(null).inflate(R.layout.txn, expenseList, false);

        incomeList.addHeaderView(openBalance,null,false);
        expenseList.addFooterView(closeBalance,null,false);

        dateSelect.setText(formatter.format(dt.toDate()));
        dateSelect.setOnClickListener(dateListener);

        //Drawable icon_write = FontIconDrawable.inflate(getActivity(), R.xml.icon_write);
        //myFAB.setImageDrawable(icon_write);

        fillData();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        dbHelper.closeDb();

    }

    public void fillData(){
        incomeList.setAdapter(new CashAdapter(getActivity(), dbHelper.fetchTransactions(dt,true)));
        incomeList.setOnItemLongClickListener(longClickListener);
        incomeList.setOnTouchListener(touchListener);

        expenseList.setAdapter(new CashAdapter(getActivity(), dbHelper.fetchTransactions(dt,false)));
        expenseList.setOnItemLongClickListener(longClickListener);
        expenseList.setOnTouchListener(touchListener);

        ((TextView) openBalance.findViewById(R.id.amount)).setText(CommonUtils.getCurrencyFormatEmpty().format(dbHelper.Cash(dt.minusDays(1))));
        ((TextView) openBalance.findViewById(R.id.name)).setText(getResources().getString(R.string.openBalance));
        ((TextView) openBalance.findViewById(R.id.name)).setTypeface(null, Typeface.BOLD_ITALIC);
        ((TextView) openBalance.findViewById(R.id.detail)).setVisibility(View.GONE);
        ((ImageView)openBalance.findViewById(R.id.bill)).setVisibility(View.GONE);
        ((FrameLayout)openBalance.findViewById(R.id.frame)).setVisibility(View.GONE);

        ((TextView)closeBalance.findViewById(R.id.amount)).setText(CommonUtils.getCurrencyFormatEmpty().format(dbHelper.Cash(dt)));
        ((TextView)closeBalance.findViewById(R.id.name)).setText(getResources().getString(R.string.closeBalance));
        ((TextView)closeBalance.findViewById(R.id.name)).setTypeface(null, Typeface.BOLD_ITALIC);
        ((TextView)closeBalance.findViewById(R.id.detail)).setVisibility(View.GONE);
        ((ImageView)closeBalance.findViewById(R.id.bill)).setVisibility(View.GONE);
        ((FrameLayout)closeBalance.findViewById(R.id.frame)).setVisibility(View.GONE);
    }

    private void showDialog(final View view, final Transaction transaction) {

        PopupMenu popupMenu = new PopupMenu(getActivity(), view);
        popupMenu.getMenuInflater().inflate(R.menu.menu_txn, popupMenu.getMenu());

        if(transaction.getAccount() == null){
           popupMenu.getMenu().findItem(R.id.view_account).setVisible(false);
        }

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.view_account:
                        Intent intent = new Intent(getActivity(), AccTxnsActivity.class);
                        intent.putExtra("accountId", transaction.getAccount().getId());
                        startActivityForResult(intent, 1);
                        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.fade_out);
                        break;
                    case R.id.delete:
                        dbHelper.deleteTransaction(transaction.getId());
                        break;
                }
                return false;
            }
        });

        popupMenu.show();
    }

}
