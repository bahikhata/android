package com.bahikhata.db;

import android.util.Log;

import com.bahikhata.model.Account;
import com.bahikhata.model.Transaction;
import com.bahikhata.model.User;

import org.joda.time.DateTime;
import java.util.Date;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by user on 29/04/17.
 */

public class DbHelper {
    Realm realm;
    RealmConfiguration realmConfiguration;

    public DbHelper(){
        /*RealmMigration realmMigration = new RealmMigration() {
            @Override
            public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
                RealmSchema schema = realm.getSchema();

                if(oldVersion == 0){
                    schema.create("Account")
                            .addField("created", Date.class)
                            .addField("id",int.class, FieldAttribute.PRIMARY_KEY)
                            .addField("updated",Date.class);

                    oldVersion++;
                }
            }
        };*/

        realmConfiguration = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();

        realm = Realm.getInstance(realmConfiguration);
    }

    public void insertUserDetails(final String name){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                User user = realm.createObject(User.class,1);
                user.setName(name);
                user.setCreated(new Date());
                user.setUpdated(new Date());
            }
        });
    }

    public void updateUserDetails(final String name){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                User oldUser = realm.where(User.class)
                        .findFirst();

                User user = new User();
                user.setId(oldUser.getId());
                user.setName(name);
                user.setUpdated(new Date());
                user.setCreated(oldUser.getCreated());

                realm.insertOrUpdate(user);
            }
        });
    }

    public User fetchUserDetails(){
        return realm.where(User.class)
                .findFirst();
    }

    public void insertTransaction(final String accName,final String detail, final Double amount, final Boolean cancelled){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Transaction transaction = realm.createObject(Transaction.class,realm.where(Transaction.class).findAll().size() + 1);

                if(accName != null){
                    Account account = realm.where(Account.class)
                            .equalTo("name",accName)
                            .findFirst();

                    if(account != null){
                        account.incrementAmount(amount);
                        transaction.setAccount(account);
                    }
                    else{
                        Account newAccount = realm.createObject(Account.class,realm.where(Account.class).findAll().size() + 1);
                        newAccount.setName(accName);
                        newAccount.setAmount(0.0);
                        newAccount.setCreated(new Date());
                        newAccount.setUpdated(new Date());
                        newAccount.setCancelled(false);
                        if(amount != null) newAccount.incrementAmount(amount);
                        transaction.setAccount(newAccount);
                    }
                }

                transaction.setCancelled(cancelled);
                transaction.setDetail(detail);
                transaction.setAmount(amount);
                transaction.setCreated(new Date());
                transaction.setUpdated(new Date());
            }
        });
    }

    public RealmResults<Transaction> fetchTransactions(DateTime dt,boolean type){
        if(type) {
            return realm.where(Transaction.class)
                    .greaterThan("created",new DateTime(dt.getYear(), dt.getMonthOfYear(), dt.getDayOfMonth(), 0, 0, 0).toDate())
                    .lessThan("created",new DateTime(dt.getYear(), dt.getMonthOfYear(), dt.getDayOfMonth(), 23, 59, 59).toDate())
                    .greaterThan("amount",0.0)
                    .findAll()
                    .sort("created");
        }
        else {
            return realm.where(Transaction.class)
                    .greaterThan("created",new DateTime(dt.getYear(), dt.getMonthOfYear(), dt.getDayOfMonth(), 0, 0, 0).toDate())
                    .lessThan("created",new DateTime(dt.getYear(), dt.getMonthOfYear(), dt.getDayOfMonth(), 23, 59, 59).toDate())
                    .lessThan("amount",0.0)
                    .findAll()
                    .sort("created");
        }
    }

    public RealmResults<Transaction> fetchTransactionByAccount(long id,boolean type){
        if (type) {
            return realm.where(Transaction.class)
                    .equalTo("account.id",realm.where(Account.class)
                            .equalTo("id",id)
                            .findFirst().getId())
                    .greaterThan("amount",0.0)
                    .findAll()
                    .sort("created");
        }
        else{
            return realm.where(Transaction.class)
                    .equalTo("account.id",realm.where(Account.class)
                            .equalTo("id",id)
                            .findFirst().getId())
                    .lessThan("amount",0.0)
                    .findAll()
                    .sort("created");
        }
    }

    public RealmResults<Transaction> fetchTransactions(){
        return realm.where(Transaction.class)
                .findAll()
                .sort("created");
    }

    public void updateTransaction(final int id, final String detail, final Boolean cancelled, final Double amount){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Transaction oldTransaction = realm.where(Transaction.class)
                        .equalTo("id",id)
                        .findFirst();

                Transaction transaction = new Transaction();
                transaction.setCancelled(cancelled);
                transaction.setDetail(detail);
                transaction.setUpdated(new Date());
                transaction.setAmount(amount);
                transaction.setCreated(oldTransaction.getCreated());

                realm.insertOrUpdate(transaction);
            }
        });


    }

    public void deleteTransaction(int id){
        final Transaction transaction = realm.where(Transaction.class)
                .equalTo("id",id)
                .findFirst();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                transaction.deleteFromRealm();
            }
        });
    }

    public void insertAccount(final String name, final Double amount, final Boolean cancelled){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Account account = realm.createObject(Account.class,realm.where(Account.class).findAll().size() + 1);

                account.setCancelled(cancelled);
                account.setName(name);
                account.setAmount(amount);
                account.setCreated(new Date());
                account.setUpdated(new Date());
            }
        });
    }

    public RealmResults<Account> fetchAccounts(){
        return realm.where(Account.class).findAll();
    }

    public Account fetchAccountById(int id){
        return realm.where(Account.class)
                .equalTo("id",id)
                .findFirst();
    }

    public void updateAccount(final int id, final Double amount, final String name){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Account oldAccount = realm.where(Account.class)
                        .equalTo("id",id)
                        .findFirst();

                Account account = new Account();
                if(amount != null){
                    account.setAmount(amount);
                }
                else{
                    account.setAmount(oldAccount.getAmount());
                }
                if(name != null) account.setName(name);
                account.setId(id);
                account.setUpdated(new Date());
                account.setCancelled(oldAccount.getCancelled());
                account.setCreated(oldAccount.getCreated());

                realm.insertOrUpdate(account);
            }
        });
    }

    public void deleteAccount(int id){
        final Account account = realm.where(Account.class)
                .equalTo("id",id)
                .findFirst();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                account.deleteFromRealm();
            }
        });
    }

    public Double Cash(DateTime dt) {

        return (Double)realm.where(Transaction.class)
                .lessThanOrEqualTo("created",new DateTime(dt.getYear(), dt.getMonthOfYear(), dt.getDayOfMonth(), 23, 59, 59).toDate())
                .equalTo("cancelled",false)
                .findAll()
                .sum("amount");

    }

    public boolean checkIfAccountExists(String to) {
        Account account = realm.where(Account.class)
                .equalTo("name",to)
                .findFirst();

        if(account == null)
            return false;
        else
            return true;
    }

    public void closeDb(){
        realm.close();
    }


}
