package com.bahikhata;

import android.content.Context;
import android.os.StrictMode;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import com.shamanland.fonticon.FontIconTypefaceHolder;
import io.realm.Realm;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by abhijeet on 07/09/15.
 */
public class MyApplication extends MultiDexApplication {
    public static Context mContext;

    Boolean DEVELOPER_MODE = true;

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = getApplicationContext();

        if(DEVELOPER_MODE){
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());
        }

        FontIconTypefaceHolder.init(getAssets(), "icons.ttf");
        JodaTimeAndroid.init(this);
        Realm.init(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
