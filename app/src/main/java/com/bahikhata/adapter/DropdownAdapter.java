package com.bahikhata.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.bahikhata.MyApplication;
import com.bahikhata.R;
import com.bahikhata.model.Account;
import com.bahikhata.util.CommonUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhijeet on 04/01/16.
 */
public class DropdownAdapter extends ArrayAdapter<Account> {
    List<Account> items;
    Context context;
    ArrayList<Account> suggestions;
    TextView name;
    TextView balance;

    public DropdownAdapter(Context context, List<Account> items) {
        super(context, -1, items);
        this.context = context;
        this.items = items;
        this.suggestions = new ArrayList<Account>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.debtors, parent,false);
        }

        name = v.findViewById(R.id.name);
        balance = v.findViewById(R.id.balance);

        name.setText(items.get(position).getName());

        if(items.get(position).getAmount() > 0){
            //if(items.get(position).getString("phone") != null)
                balance.setTextColor(ContextCompat.getColor(MyApplication.mContext,R.color.minus));
            balance.setText(CommonUtils.getCurrencyFormat().format(items.get(position).getAmount()));
        }
        else if(items.get(position).getAmount() < 0){
            //if(items.get(position).getString("phone") != null)
                balance.setTextColor(ContextCompat.getColor(MyApplication.mContext,R.color.plus));
            balance.setText(CommonUtils.getCurrencyFormat().format(-items.get(position).getAmount()));
        }
        else{
            balance.setText(CommonUtils.getCurrencyFormat().format(0));
        }

        //balance.setText(CommonUtils.getCurrencyFormat().format(items.get(position).getNumber("balance")));

        return v;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((Account)(resultValue)).getName();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            if(charSequence != null){
                suggestions.clear();
                Log.e("Items",items.get(0).getName());
                for(int i=0;i<items.size();i++){
                    Account account = items.get(i);
                    if(account.getName().toLowerCase().startsWith(charSequence.toString().toLowerCase())){
                        suggestions.add(account);
                    }
                }
                /*for(Account account:items){
                    if(account.getName().toLowerCase().startsWith(charSequence.toString().toLowerCase())){
                        suggestions.add(account);
                    }
                }*/
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            }
            else{
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            ArrayList<Account> filteredList = (ArrayList<Account>) filterResults.values;
            if(filterResults != null && filterResults.count > 0){
                clear();
                for(Account account: filteredList){
                    add(account);
                }
                notifyDataSetChanged();
            }
        }
    };
}
