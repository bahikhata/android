package com.bahikhata.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bahikhata.R;
import com.bahikhata.activity.ImageActivity;
import com.bahikhata.model.Transaction;
import com.bahikhata.util.CommonUtils;

import java.util.List;
/**
 * Created by abhijeet on 04/01/16.
 */
public class AccTxnsAdapter extends ArrayAdapter<Transaction> {
    List<Transaction> items;
    Context context;
    TextView amount;
    TextView detail;
    TextView date;
    ImageView bill;
    FrameLayout frame;

    public AccTxnsAdapter(Context context, List<Transaction> items) {
        super(context, -1, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.txn_credit, parent, false);
        }
        amount = v.findViewById(R.id.amount);
        detail = (TextView)v.findViewById(R.id.detail);
        date = (TextView)v.findViewById(R.id.date);
        bill = (ImageView)v.findViewById(R.id.bill);
        frame = (FrameLayout)v.findViewById(R.id.frame);

        amount.setText(items.get(position).getAmount() > 0 ? CommonUtils.getCurrencyFormatEmpty().format(items.get(position).getAmount())
                : CommonUtils.getCurrencyFormatEmpty().format(-items.get(position).getAmount()));
        detail.setText(items.get(position).getDetail());
        date.setText(CommonUtils.getDateFormatWY().format(items.get(position).getCreated()));

        //Strikethrough in case of cancelled
        if (items.get(position).getCancelled()) {
            amount.setPaintFlags(amount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            date.setPaintFlags(date.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            detail.setPaintFlags(detail.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            amount.setPaintFlags(amount.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            date.setPaintFlags(date.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            detail.setPaintFlags(detail.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            amount.setPaintFlags(0);
            date.setPaintFlags(0);
            detail.setPaintFlags(0);
        }

        bill.setVisibility(View.GONE);
        frame.setVisibility(View.GONE);

        /*bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (items.get(position).getBytes("image") != null &&
                        !items.get(position).getBoolean("cancelled")) {
                    Intent intent = new Intent(context.getApplicationContext(), ImageActivity.class);
                    intent.putExtra("image", items.get(position).getBytes("image"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.getApplicationContext().startActivity(intent);
                }

            }
        });

        byte[] image = items.get(position).getBytes("image");
        if (image != null && !items.get(position).getBoolean("cancelled")) {
            bill.setVisibility(View.VISIBLE);
            bill.setImageBitmap(BitmapFactory.decodeByteArray(image, 0, image.length));
            frame.setVisibility(View.VISIBLE);
        } else {
            //hide imageview
            bill.setVisibility(View.GONE);
            frame.setVisibility(View.GONE);
        }*/

        return v;
    }
}
