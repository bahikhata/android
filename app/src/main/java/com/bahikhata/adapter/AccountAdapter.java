package com.bahikhata.adapter;

import android.content.Context;
import android.graphics.Paint;
import androidx.core.content.ContextCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bahikhata.MyApplication;
import com.bahikhata.R;
import com.bahikhata.model.Account;
import com.bahikhata.util.CommonUtils;

import java.util.List;

/**
 * Created by abhijeet on 04/01/16.
 */
public class AccountAdapter extends ArrayAdapter<Account> {
    TextView name;
    TextView balance;
    List<Account> items;
    Context context;

    public AccountAdapter(Context context, List<Account> items) {
        super(context, -1, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.debtors, parent, false);
        }

        name = (TextView)v.findViewById(R.id.name);
        balance = (TextView)v.findViewById(R.id.balance);

        name.setText(items.get(position).getName());
        balance.setText(CommonUtils.getCurrencyFormat().format(items.get(position).getAmount()));

        if(items.get(position).getAmount() > 0){
            balance.setTextColor(ContextCompat.getColor(MyApplication.mContext,R.color.minus));
        }
        else if(items.get(position).getAmount() < 0){
            balance.setTextColor(ContextCompat.getColor(MyApplication.mContext,R.color.plus));
            balance.setText(CommonUtils.getCurrencyFormat().format(-items.get(position).getAmount()));
        }
        else{
            balance.setText(CommonUtils.getCurrencyFormat().format(0));
        }

        if (items.get(position).getCancelled()) {
            name.setPaintFlags(name.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            balance.setPaintFlags(balance.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            name.setPaintFlags(name.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            balance.setPaintFlags(balance.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            name.setPaintFlags(0);
            balance.setPaintFlags(0);
        }

        return v;
    }

    public void setItems(List<Account> items) {
        this.items = items;
        notifyDataSetChanged();
    }
}
