package com.bahikhata.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bahikhata.R;
import com.bahikhata.activity.ImageActivity;
import com.bahikhata.model.Account;
import com.bahikhata.model.Transaction;
import com.bahikhata.util.CommonUtils;

import java.util.List;

/**
 * Created by abhijeet on 04/01/16.
 */
public class CashAdapter extends ArrayAdapter<Transaction> {
    List<Transaction> items;
    Context context;
    TextView amount;
    TextView detail;
    TextView name;
    ImageView bill;
    FrameLayout frame;

    public CashAdapter(Context context, List<Transaction> items) {
        super(context, -1, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.txn, parent, false);
        }
        amount = (TextView)v.findViewById(R.id.amount);
        detail = (TextView)v.findViewById(R.id.detail);
        name = (TextView)v.findViewById(R.id.name);
        bill = v.findViewById(R.id.bill);
        frame = v.findViewById(R.id.frame);

        amount.setText(items.get(position).getAmount() > 0 ? CommonUtils.getCurrencyFormatEmpty().format(items.get(position).getAmount())
                : CommonUtils.getCurrencyFormatEmpty().format(-items.get(position).getAmount()));
        Account account = items.get(position).getAccount();

        if (account != null) {
            if (!account.getName().isEmpty()) {
                name.setText(account.getName());
            } else {
                name.setVisibility(View.GONE);
            }
        } else {
            name.setVisibility(View.GONE);
        }

        if (items.get(position).getDetail().equals("")) {
            detail.setVisibility(View.GONE);
        } else {
            detail.setText(items.get(position).getDetail());
        }

        //Strikethrough in case of cancelled
        if (items.get(position).getCancelled()) {
            amount.setPaintFlags(amount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            name.setPaintFlags(name.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            detail.setPaintFlags(detail.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            amount.setPaintFlags(amount.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            name.setPaintFlags(name.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            detail.setPaintFlags(detail.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            amount.setPaintFlags(0);
            name.setPaintFlags(0);
            detail.setPaintFlags(0);
        }

        bill.setVisibility(View.GONE);
        frame.setVisibility(View.GONE);

        /*bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (items.get(position).getBytes("image") != null &&
                        !items.get(position).getCancelled()) {
                    Intent intent = new Intent(context.getApplicationContext(), ImageActivity.class);
                    intent.putExtra("image", items.get(position).getBytes("image"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.getApplicationContext().startActivity(intent);
                }

            }
        });

        byte[] image = items.get(position).getBytes("image");
        if (image != null && !items.get(position).getCancelled()) {
            bill.setVisibility(View.VISIBLE);
            bill.setImageBitmap(BitmapFactory.decodeByteArray(image, 0, image.length));
            frame.setVisibility(View.VISIBLE);
        } else {
            //hide imageview
            bill.setVisibility(View.GONE);
            frame.setVisibility(View.GONE);
        }*/

        return v;
    }
}
