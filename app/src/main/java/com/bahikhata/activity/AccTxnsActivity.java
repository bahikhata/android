package com.bahikhata.activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.bahikhata.MyApplication;
import com.bahikhata.R;
import com.bahikhata.adapter.AccTxnsAdapter;
import com.bahikhata.db.DbHelper;
import com.bahikhata.model.Account;
import com.bahikhata.util.CommonUtils;

import org.joda.time.DateTime;

import java.text.NumberFormat;
import java.util.Locale;

public class AccTxnsActivity extends AppCompatActivity {
    ListView crList;
    ListView dbList;
    Toolbar toolbar;
    TextView balance;

    static NumberFormat currencyFormat;
    static int accountId;
    String phoneNumber;
    DbHelper dbHelper;
    static DateTime dt = new DateTime();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acc_txns);
        crList = (ListView)findViewById(R.id.in);
        dbList = (ListView)findViewById(R.id.out);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        balance = (TextView)findViewById(R.id.balance);

        dbHelper = new DbHelper();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        currencyFormat = NumberFormat.getCurrencyInstance(
                new Locale(getResources().getString(R.string.language),
                        getResources().getString(R.string.country)));

        accountId = getIntent().getIntExtra("accountId",0);

        Account account = dbHelper.fetchAccountById(accountId);
        //Log.e("Account", account.getPhone());
        getSupportActionBar().setTitle(account.getName() + " " + account.getPhone());

        String str = "Balance : " + CommonUtils.getCurrencyFormat().format(account.getAmount());
        balance.setText(str);

        dbList.setAdapter(new AccTxnsAdapter(MyApplication.mContext, dbHelper.fetchTransactionByAccount(accountId,false)));
        crList.setAdapter(new AccTxnsAdapter(MyApplication.mContext, dbHelper.fetchTransactionByAccount(accountId,true)));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        /*else if(id == R.id.remind){
            //Show date picker.
            //Once date picked... set a alarm for that date.
            //When alarm is raised... show notification.
            //Click on notification .. brings inside the app.

            //newFragment.getDialog().setTitle("Remind me on");
            //
            am = (AlarmManager)(this.getSystemService(Context.ALARM_SERVICE));

            DialogFragment newFragment = new DatePickerFragment();
            newFragment.show(getFragmentManager(), "datePicker");
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_account, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        final MenuItem callItem = menu.findItem(R.id.call);



        /*List<Account> accounts = realm.where(Account.class).findAll();

        Double sum = accounts.get(0).getAmount();
        amountItem.setTitle(CommonUtils.getCurrencyFormat().format(sum < 0 ? -sum:sum));*/

        /*phoneNumber = parseObject.getString("phone");
        if(phoneNumber != null){
            FontIconDrawable drawable = FontIconDrawable.inflate(MyApplication.mContext,R.xml.icon_call);
            callItem.setIcon(drawable);
            callItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+phoneNumber));
                    startActivity(intent);
                    return false;
                }
            });
        }*/

        //

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.fade_in, R.anim.slide_out_right);
        finish();
    }
}
