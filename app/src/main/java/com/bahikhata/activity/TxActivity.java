package com.bahikhata.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.ExifInterface;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import com.bahikhata.MyApplication;
import com.bahikhata.R;
import com.bahikhata.adapter.DropdownAdapter;
import com.bahikhata.db.DbHelper;
import com.bahikhata.util.CommonUtils;

import org.joda.time.DateTime;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class TxActivity extends AppCompatActivity {
    EditText amount;
    EditText detail;
    ImageView bill;
    Button inBtn;
    Button outBtn;
    Toolbar toolbar;
    AutoCompleteTextView account;
    DateTime dt;
    static SimpleDateFormat formatter = new SimpleDateFormat("dd MMM");
    SoundPool soundPool;
    int soundID;
    boolean loaded = false;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int RQS_PICK_CONTACT = 2;
    static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    String phoneNumber = "";
    DbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tx);
        amount = (EditText)findViewById(R.id.amount);
        detail = (EditText)findViewById(R.id.detail);
        bill = (ImageView)findViewById(R.id.bill);
        inBtn = (Button)findViewById(R.id.inBtn);
        outBtn = (Button)findViewById(R.id.outBtn);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        account = (AutoCompleteTextView)findViewById(R.id.account);

        dbHelper = new DbHelper();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.title_Transaction));

        inBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTx(v);
            }
        });
        outBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTx(v);
            }
        });

        //personId = getIntent().getStringExtra("personId");
        dt = new DateTime();

        amount.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(amount, InputMethodManager.SHOW_IMPLICIT);

        //amount.addTextChangedListener(new CurrencyWatcher());

        //loadSound(R.raw.coins);

        bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Close keyboard if open.
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                //Open the Camera for result and when camera closes then put the picture clicked in place of the placeholder image now..
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
            }
        });

        DropdownAdapter adapter = new DropdownAdapter(this,dbHelper.fetchAccounts());
        account.setAdapter(adapter);

        account.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("Adapter", adapterView.getAdapter().getItem(i).toString());
            }
        });

        //MyApplication.mixpanel.track("Transaction Started");
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Bitmap retVal;

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

        return retVal;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            //Get path of the photo clicked.
            Uri tempUri = getImageUri(getApplicationContext(), imageBitmap);

            //Change the orientation of the image if it is wrong.
            try {
                ExifInterface ei = new ExifInterface(getRealPathFromURI(tempUri));
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

                Log.e("Orientation", String.valueOf(orientation));

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotateImage(imageBitmap, 90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotateImage(imageBitmap, 180);
                        break;
                }
                bill.setImageBitmap(imageBitmap);
            } catch (IOException e) {
                Log.e("Exception", "Error");
            }
        }
        else if(requestCode == RQS_PICK_CONTACT){
            if(resultCode == RESULT_OK){
                Uri contactData = data.getData();
                ContentResolver contentResolver = getContentResolver();
                Cursor cursor =  contentResolver.query(contactData, null, null, null, null);
                cursor.moveToFirst();
                String name = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
                String id = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                if(Integer.parseInt(cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.HAS_PHONE_NUMBER)))>0){
                    try{
                        Cursor cursor1 = contentResolver.query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id},null);
                        while (cursor1.moveToNext()){
                            phoneNumber = cursor1.getString(cursor1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));

                        }
                        cursor1.close();
                    }
                    catch (Exception exception){
                        Log.e("Ex",exception.getLocalizedMessage());
                    }
                }
                account.setText(name);
                cursor.close();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == PERMISSIONS_REQUEST_READ_CONTACTS){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                pickContact(null);
            }
        }
    }

    public void openCalculator(View view) {
        closeKeyboardIfOpen();

        ArrayList<HashMap<String, Object>> items = new ArrayList<HashMap<String, Object>>();
        PackageManager pm = getPackageManager();
        List<PackageInfo> packs = pm.getInstalledPackages(0);
        for (PackageInfo pi : packs) {
            if (pi.packageName.toString().toLowerCase().contains("calcul")) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("appName", pi.applicationInfo.loadLabel(pm));
                map.put("packageName", pi.packageName);
                items.add(map);
            }
        }
        if (items.size() >= 1) {
            String packageName = (String) items.get(0).get("packageName");
            Intent i = pm.getLaunchIntentForPackage(packageName);
            if (i != null)
                startActivity(i);
        } else {
            // Application not found

        }
    }

    public void closeKeyboardIfOpen() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void pickContact(View view){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && checkSelfPermission(android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},PERMISSIONS_REQUEST_READ_CONTACTS);

        }
        else{
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(intent, RQS_PICK_CONTACT);
        }
    }

    class CurrencyWatcher implements TextWatcher {
        private String current = "";

        public CurrencyWatcher() {

        }

        @Override
        public void afterTextChanged(Editable s) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!s.toString().equals(current)) {
                amount.removeTextChangedListener(this);

                String cleanString = s.toString().replaceAll("[Rs.$,\\s]", "");

                Double parsed = cleanString.equals("") ? 0 : Double.parseDouble(cleanString);
                String formatted = CommonUtils.getCurrencyFormatEmpty().format(parsed);
                current = formatted;

                amount.setText(formatted);
                amount.setSelection(formatted.length());

                amount.addTextChangedListener(this);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        } else if (id == R.id.date) {

            closeKeyboardIfOpen();

            Calendar newCalendar = Calendar.getInstance();

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    dt = new DateTime(year, monthOfYear + 1, dayOfMonth, dt.getHourOfDay(), dt.getMinuteOfHour(), dt.getSecondOfMinute());
                    invalidateOptionsMenu();
                }
            }, newCalendar.get(Calendar.YEAR)
                    , newCalendar.get(Calendar.MONTH)
                    , newCalendar.get(Calendar.DAY_OF_MONTH));

            datePickerDialog.getDatePicker().setMaxDate(new DateTime().toDate().getTime());
            datePickerDialog.show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_addtx, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        dateIcon(menu.findItem(R.id.date));

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        //overridePendingTransition(R.anim.fade_in,R.anim.slide_out_right);
        setResult(RESULT_CANCELED);
        try {
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addTx(View view) {
        if (amount.getText().toString().isEmpty() || amount.getText().toString().length() > 15) {
            amount.setError(getString(R.string.error_required));
        }
        else {
            Double amt = Double.valueOf(amount.getText().toString());
            if (view.getId() == R.id.inBtn)
                dbHelper.insertTransaction(account.getText().toString().isEmpty() ? null : account.getText().toString(),detail.getText().toString(),amt,false);
            else
                dbHelper.insertTransaction(account.getText().toString().isEmpty() ? null : account.getText().toString(), detail.getText().toString(),-amt,false);
            finish();
        }
    }

    public void AddTransaction(final Double amount, final String to, final String detail, final DateTime datetime) {

        /*Account account = realm.where(Account.class).findFirst();
        if(account == null) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Account account = realm.createObject(Account.class);

                    account.setName(to);
                    account.setCreated(new Date());
                    account.setUpdated(new Date());
                    account.setAmount(0.0);
                }
            });
        }*/

                    /*final ParseObject transaction = new ParseObject("Transaction");
                    transaction.put("user", ParseUser.getCurrentUser());
                    if (list.size() == 0) {
                        ParseObject person = new ParseObject("Account");
                        person.put("user", ParseUser.getCurrentUser());
                        if(!phoneNumber.isEmpty()){
                            person.put("phone",phoneNumber);
                        }
                        person.put("name", to);
                        person.put("balance", amount);
                        person.pinInBackground();
                        person.saveEventually();
                        transaction.put("person", person);
                    }
                    else{
                        list.get(0).increment("balance", amount);
                        transaction.put("person", list.get(0));
                    }
                    transaction.put("am ount", amount);
                    transaction.put("detail", detail);
                    transaction.put("date", datetime.toDate());
                    transaction.put("mode", 0);

                    if (bill.getDrawable() != null) {
                        Bitmap bitmap = ((BitmapDrawable) bill.getDrawable()).getBitmap();
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] image = stream.toByteArray();

                        transaction.put("image", image);
                        transaction.saveEventually();
                        transaction.pinInBackground();
                        overridePendingTransition(R.anim.fade_in, R.anim.slide_out_right);

                        playSound();

                        Intent output = new Intent();
                        output.putExtra("date", datetime.toString());
                        setResult(RESULT_OK, output);
                        //setResult(1);

                        // Analytics track the event.
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("image",true);
                        } catch (JSONException e1) {
                            e.printStackTrace();
                        }
                        MyApplication.mixpanel.track("Transaction Done",jsonObject);
                        phoneNumber = "";

                        finish();
                    } else {
                        transaction.saveEventually();
                        transaction.pinInBackground();
                        overridePendingTransition(R.anim.fade_in, R.anim.slide_out_right);

                        playSound();

                        Intent output = new Intent();
                        output.putExtra("date", datetime.toString());
                        setResult(RESULT_OK, output);

                        // Analytics track the event.
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("image",false);
                        } catch (JSONException e1) {
                            e.printStackTrace();
                        }
                        MyApplication.mixpanel.track("Transaction Done",jsonObject);
                        //setResult(1);
                        phoneNumber = "";
                        finish();
                    }
                } else {
                    Log.e("error", e.getMessage());
                }
            }
        });*/
    }

    private void dateIcon(MenuItem dateItem){
        String date = formatter.format(dt.toDate());
        String data[] = date.split(" ");

        Bitmap b = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);

        Rect r = new Rect(0, 0, 100, 40);
        Paint mPaint = new Paint();
        mPaint.setStrokeWidth(2);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(ContextCompat.getColor(this, R.color.white));

        c.drawRect(r, mPaint);

        r = new Rect(0, 0, 100, 100);
        mPaint.setStrokeWidth(5);
        mPaint.setStyle(Paint.Style.STROKE);
        c.drawRect(r, mPaint);

        TextPaint textPaint = new TextPaint(TextPaint.ANTI_ALIAS_FLAG);
        textPaint.setColor(ContextCompat.getColor(this, R.color.primary_dark));
        textPaint.setTextSize(28f);
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        c.drawText(data[1], 27, 30, textPaint);

        textPaint.setColor(ContextCompat.getColor(this, R.color.white));
        textPaint.setTextSize(34f);
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        c.drawText(data[0], 34, 80, textPaint);

        BitmapDrawable drawable = new BitmapDrawable(getResources(), b);
        dateItem.setIcon(drawable);
    }

    public void playSound() {
        if (loaded) {
            soundPool.play(soundID, 1, 1, 1, 0, 1f);
        }
    }

    private int loadSound(int filename) {
        int soundID = 0;
        if (soundPool == null) {
            soundPool = buildSoundPool();
        }
        try {
            soundID = soundPool.load(MyApplication.mContext, filename, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return soundID;
    }

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private SoundPool buildSoundPool() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            soundPool = new SoundPool.Builder()
                    .setMaxStreams(25)
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else {
            buildBeforeAPI21();
        }
        return soundPool;
    }

    public void buildBeforeAPI21() {
        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                loaded = true;
            }
        });

        soundID = soundPool.load(MyApplication.mContext, R.raw.coins, 1);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbHelper.closeDb();
    }
}
