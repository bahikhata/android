package com.bahikhata.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.bahikhata.R;

public class LaunchActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
