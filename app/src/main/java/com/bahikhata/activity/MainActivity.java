package com.bahikhata.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bahikhata.MyApplication;
import com.bahikhata.R;
import com.bahikhata.adapter.ViewPagerAdapter;
import com.bahikhata.db.DbHelper;
import com.bahikhata.fragment.AccountFragment;
import com.bahikhata.fragment.CashFragment;

import info.androidhive.fontawesome.FontDrawable;
import info.androidhive.fontawesome.FontTextView;

/**
 * Created by abhijeet on 04/01/16.
 */
public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;
    ViewPager viewPager;
    TabLayout tabLayout;
    ProgressBar spinner;
    TextView logoutText;
    View background;
    DrawerLayout mDrawer;
    NavigationView nvDrawer;

    ActionBarDrawerToggle drawerToggle;
    ViewPagerAdapter adapter;
    boolean isStart;
    TextView name,phone;
    SharedPreferences sharedPreferences;

    DbHelper dbHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs);
        spinner = findViewById(R.id.progressBar);
        logoutText = findViewById(R.id.logoutText);
        background = findViewById(R.id.back);

        setSupportActionBar(toolbar);
        toolbar.setTitle("");

        dbHelper = new DbHelper();

        firstTimer();

        setupPagerAdapter();

        spinner.setVisibility(View.GONE);
        logoutText.setVisibility(View.GONE);
        background.setVisibility(View.GONE);

        // Find our drawer_menu view
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawer.addDrawerListener(drawerToggle);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);

        // Setup drawer_menu view
        setupDrawer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*if(mService != null){
            unbindService(mServiceConn);
        }*/
        dbHelper.closeDb();
    }

    public void setupPagerAdapter() {
        viewPager.setOffscreenPageLimit(2);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new CashFragment(), getString(R.string.menu_cash));
        adapter.addFragment(new AccountFragment(), getString(R.string.menu_account));
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(0);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition() == 0){
                    viewPager.setCurrentItem(0);
                }
                else if(tab.getPosition() == 1){
                    viewPager.setCurrentItem(1);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        /*for(int i=0;i<1;i++){
            dbHelper.insertTransaction("A new Txn",1000.0,false);
        }*/
    }

    public void firstTimer(){
        sharedPreferences = getPreferences(MODE_PRIVATE);
        isStart = sharedPreferences.getBoolean("start", true) &&
                (dbHelper.fetchUserDetails() == null);

        if(isStart){
            setName(null);
        }
    }

    public void setName(final String prefill){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View dialogView = this.getLayoutInflater().inflate(R.layout.dialog_detail,null);
        final EditText name = (EditText)dialogView.findViewById(R.id.businessName);
        if(prefill != null){
            name.setText(prefill);
            name.setSelection(name.getText().length());
        }

        builder.setView(dialogView)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(name.getText().toString().isEmpty()){
                            name.setError(getString(R.string.error_name));
                        }
                        else{
                            if(prefill == null){
                                dbHelper.insertUserDetails(name.getText().toString());

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean("start",false);
                                editor.apply();
                                Snackbar.make(viewPager,
                                        "Welcome " + dbHelper.fetchUserDetails().getName(),
                                        Snackbar.LENGTH_SHORT).show();
                            }
                            else
                                dbHelper.updateUserDetails(name.getText().toString());

                            // Change the business name.
                            setupDrawer();

                            dialog.dismiss();
                        }
                    }
                });

        builder.setTitle(getString(R.string.title_detail));

        // Create the AlertDialog object and return it
        Dialog d = builder.create();
        // Show keyboard on dialog open.
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        d.show();
    }

    public void setupDrawer() {
        //FontIconTextView logout = (FontIconTextView) nvDrawer.findViewById(R.id.logout);
        FontTextView share = nvDrawer.findViewById(R.id.share);
        //FontIconTextView support = nvDrawer.findViewById(R.id.support);
        FontTextView setting = nvDrawer.findViewById(R.id.setting);
        name = (TextView) nvDrawer.findViewById(R.id.name);

        //Drawable icon_logout = FontIconDrawable.inflate(this, R.xml.icon_logout);

        FontDrawable icon_share = new FontDrawable(this, R.string.fa_font_awesome, false, true);
        FontDrawable icon_setting = new FontDrawable(this, R.string.fa_font_awesome, false, true);
        //Drawable icon_share = FontIconDrawable.inflate(this, R.xml.icon_share);
        //Drawable icon_support = FontIconDrawable.inflate(this, R.xml.icon_support);
        //Drawable icon_setting = FontIconDrawable.inflate(this, R.xml.icon_share);

        //support.setCompoundDrawables(null, icon_support, null, null);
        //share.setCompoundDrawables(null, icon_share, null, null);
        //logout.setCompoundDrawables(null, icon_logout, null, null);
        //setting.setCompoundDrawables(null,icon_setting,null,null);

        if(dbHelper.fetchUserDetails() != null){
            name.setText(dbHelper.fetchUserDetails().getName());
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer_menu toggles
        //drawerToggle.onConfigurationChanged(newConfig);
    }

    public void menuItemClicked(View view) {
        if(view.getId() == R.id.editName){
            setName(dbHelper.fetchUserDetails() != null ? dbHelper.fetchUserDetails().getName():null);
        }
        else if (view.getId() == R.id.share) {
            /*Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
            whatsappIntent.setType("text/plain");
            whatsappIntent.setPackage("com.whatsapp");
            whatsappIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_message));
            try {
                startActivity(whatsappIntent);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(MyApplication.mContext, "Whatsapp have not been installed.", Toast.LENGTH_LONG).show();
            }*/
            Intent intent = new Intent(this,SampleActivity.class);
            startActivity(intent);
        }/*
        else if (view.getId() == R.id.support) {
            //Support.setUserIdentifier(ParseUser.getCurrentUser().getString("name"));
        }*/
        else if(view.getId() == R.id.setting) {
            Intent intent = new Intent(this,SettingsActivity.class);
            startActivity(intent);
        }
        /*else if (view.getId() == R.id.logout) {
            //show logout text and spinner.
            logoutText.setText(getString(R.string.logging_out));
            background.setVisibility(View.VISIBLE);
            spinner.setVisibility(View.VISIBLE);
            logoutText.setVisibility(View.VISIBLE);

            //Put this in another thread.
            new Handler().post(new Runnable() {
                @Override
                public void run() {

                    // Logout of twitter session.
                    //Digits.getSessionManager().clearActiveSession();cxxxffc

                    //Clear shared Preference.
                    SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.clear();
                    editor.apply();

                    finish();
                }
            });

        }*/
        mDrawer.closeDrawers();
    }

    public void showMenu(View view) {
        mDrawer.openDrawer(GravityCompat.START);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        //drawerToggle.syncState();
    }

    public void AddTxClick(View view) {
        Intent intent = new Intent(this, TxActivity.class);
        startActivityForResult(intent, 0);
        overridePendingTransition(R.anim.slide_in_right,R.anim.fade_out);
    }

    public void AddAccountClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View dialogView = this.getLayoutInflater().inflate(R.layout.dialog_newacc,null);
        final EditText name = (EditText)dialogView.findViewById(R.id.accName);
        builder.setView(dialogView)
                .setTitle(getString(R.string.addAccount_title))
                .setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(name.getText().toString().isEmpty()) {
                            name.setError(getString(R.string.error_name));
                        }
                        else if(dbHelper.checkIfAccountExists(name.getText().toString())){
                            name.setError(getString(R.string.error_account_multiple));
                        }
                        else{
                            dbHelper.insertAccount(name.getText().toString(),0.0,false);
                            dialog.dismiss();
                        }
                    }
                });

        // Create the AlertDialog object and return it
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        d.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == RESULT_OK && data != null) {
            try {
                ((CashFragment) adapter.getItem(0)).fillData();
                ((AccountFragment) adapter.getItem(1)).fillData();
                Log.e("sds","Filled Data");
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

}
