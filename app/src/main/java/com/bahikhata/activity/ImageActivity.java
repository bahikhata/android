package com.bahikhata.activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import androidx.core.app.NotificationCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;

import com.bahikhata.R;
import com.shamanland.fonticon.FontIconButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageActivity extends AppCompatActivity {

    private File downloadDirectory;
    private byte[] image;
    private String number;
    private File billImage;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        imageView = (ImageView)findViewById(R.id.iv_bill);
        image = getIntent().getByteArrayExtra("image");

        FontIconButton downloadButton = (FontIconButton) findViewById(R.id.fib_download);

        imageView.setImageBitmap(BitmapFactory.decodeByteArray(image, 0, image.length));

        downloadDirectory = new File(Environment.getExternalStorageDirectory()
                + "/bahikhata");
        if (!downloadDirectory.exists()) {
            downloadDirectory.mkdirs();
        }

        number = getNumber(downloadDirectory);

        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadFile();
            }
        });
    }

    private void downloadFile() {
        billImage = new File(downloadDirectory, "Bill" + number + ".png");
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(billImage.getPath());
            fileOutputStream.write(image);
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        showNotification();
    }

    private void showNotification() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("Bahikhata")
                        .setContentText("Bill" + number + " downloaded");


        Intent toLaunch = new Intent();
        toLaunch.setAction(Intent.ACTION_VIEW);
        toLaunch.setDataAndType(Uri.fromFile(new File(billImage.getPath())),
                MimeTypeMap.getSingleton().getMimeTypeFromExtension("png"));

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, toLaunch, 0);

        mBuilder.setContentIntent(contentIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(100, mBuilder.build());
    }

    private String getNumber(File dir) {

        int i = 0;
        for (File f : dir.listFiles()) {
            if (f.getName().startsWith("Bill")) {
                i++;
            }
        }

        if (i == 0) {
            return "";
        } else {
            return String.valueOf(i);
        }
    }
}
