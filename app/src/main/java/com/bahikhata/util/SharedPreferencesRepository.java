package com.bahikhata.util;

/**
 * Created by abhijeet on 24/07/16.
 */

public interface SharedPreferencesRepository {
    String ONBOARDING_DONE = "onboard";

    boolean isOnboardingDone();

}
