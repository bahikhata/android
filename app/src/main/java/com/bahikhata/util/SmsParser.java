package com.bahikhata.util;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by abhi on 06/08/16.
 */
public class SmsParser {

    public SmsParser(){

    }

    public void readSMS() {
        /*Cursor cursor = getContentResolver().query(Uri.parse("content://sms/"), null, null, null, null);

        try {
            if (cursor.moveToFirst()) { // must check the result to prevent exception
                do {
                    String msgData = "";

                    for (int idx = 0; idx < cursor.getColumnCount(); idx++) {
                        msgData += " " + cursor.getColumnName(idx) + ":" + cursor.getString(idx);
                        Log.e("SMS", msgData);
                    }
                    // use msgData
                } while (cursor.moveToNext());
                cursor.close();

            } else {
                // empty box, no SMS
                Log.e("Empty", "No SMS");
            }
        } catch (NullPointerException e) {
            Log.e("Empty", e.getMessage());
        }*/
    }

    public void parseSMS(){
        String sms = "You have sent UGX10,000 to 08970000.\n" +
                "Reason:j.\n" +
                "Your balance is UGX120,000.\n" +
                "Thank you for using KKL MobileMoney.";

        Map<String, String> data = parseSms(sms);
        //saveToDB(data);

        Log.e("Parsed Data", data.toString());
    }

    private Map<String, String> parseSms(String s){
        Map<String, String> ret = new HashMap<>();
        s = s.replace("\n", "");
        StringTokenizer t = new StringTokenizer(s, ".");
        while (t.hasMoreTokens()){
            String b = t.nextToken().trim();
            if (b.startsWith("You have sent") ||  (b.startsWith("Airtime") || (b.startsWith("You have received")))){
                String type = getType(b);
                String parsed = parseAmount(b);
                String number = parseNumber(b);
                ret.put("amount", parsed);
                ret.put("number", number);
                ret.put("type", type);
            }else if(b.startsWith("Your")){//balance
                String parsed = parseAmount(b);
                ret.put("balance", parsed);
            }else if (b.startsWith("Reason")){
                ret.put("reason", b.toString());
            }
        }

        return ret;
    }

    private String getType(String s){
        if (s.startsWith("You have sent"))//Use your constants
            return "Payment";
        else if (s.startsWith("Airtime"))
            return "Air time";
        else if (s.startsWith("You have received"))
            return "Deposit";

        return "Unknown";
    }

    private String parseNumber(String s){
        String numberFragment = s.substring(s.lastIndexOf(' '), s.length());//extract number
        return numberFragment;
    }

    private String parseAmount(String s){
        char[] arr = s.toCharArray();
        StringBuffer sb = new StringBuffer();
        boolean parsingNumber = false;
        for (char c: arr){
            if (Character.isDigit(c))
                parsingNumber = true;
            if (Character.isLetter(c)  && c != ',' )
                parsingNumber = false;
            if (parsingNumber && c == ' ')//we have reached end of digit series
                break; //done
            if (parsingNumber)
                sb.append(c);
        }

        return sb.toString();
    }
}
