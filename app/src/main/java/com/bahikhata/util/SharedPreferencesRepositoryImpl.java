package com.bahikhata.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by abhijeet on 24/07/16.
 */

public class SharedPreferencesRepositoryImpl implements SharedPreferencesRepository {
    private SharedPreferences prefs;

    public SharedPreferencesRepositoryImpl(Context context){
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public boolean isOnboardingDone() {
        return prefs.getBoolean(SharedPreferencesRepository.ONBOARDING_DONE,false);
    }

}
